<?php 
	$args = array(
		'post_type' => 'consultor',
		'orderby' => 'publish_date',
		'order' => 'ASC'
	);
	$the_query = new WP_Query( $args);
?>

<div id="equipo"></div>
<div class="equipo pt-5">
	<div class="container pt-5">

		<div class="row">

			<div class="col-lg-6 wow fadeIn">
				<div class="">
					<h1 class="gray-font">EQUIPO DE</h1>
					<h1 class="ml-5">CONSULTORES</h1>
				</div>
				
				<div class="slider-for-2 mt-5">
					<!-- <div> <h3 class="sans text-right blue-font">Arq. Jorge Adolfo Martínez</h3><h5 class="sans light text-right blue-font">Consultor de Ventas</h5> </div> -->
					<?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div> <h3 class="sans text-right blue-font"><?php the_title(); ?></h3><h5 class="sans light text-right blue-font"><?php the_content(); ?></h5> </div>
					<!-- <div> <h3 class="sans text-right blue-font">Mtra. Cristina Reinosa</h3><h5 class="sans light text-right blue-font">Consultora en Mercadotecnia y Ventas</h5> </div>
					<div> <h3 class="sans text-right blue-font">Mtra. Xóchitlquetzalli Quiróz</h3><h5 class="sans light text-right blue-font">Consultor Jurídico</h5> </div>
					<div> <h3 class="sans text-right blue-font">Mtra. Gloria Soancatl Mendoza</h3><h5 class="sans light text-right blue-font">Consultor en Finanzas</h5> </div>
					<div> <h3 class="sans text-right blue-font">Lic. Mariana Massiel</h3><h5 class="sans light text-right blue-font">Consultor en Diseño e Imagen Corporativa</h5> </div>
					<div> <h3 class="sans text-right blue-font">Mtro. Miguel Ángel Cristales Hoyos</h3><h5 class="sans light text-right blue-font">Consultor en Modelos de Negocios <br> y Administración</h5> </div>
 -->
					<?php endwhile; else: 
					?>
					<?php endif; 
					?>
				</div>

			</div>

			<div class="col-lg-6">
				<div class="slider-for">
					<!-- <div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/JorgeAdolfo.jpg" alt=""></div> -->
					<?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
					<div><img class="big-img" src="<?php echo $url?>" alt=""></div>
					<!-- <div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/CristinaReinosa.jpg" alt=""></div>
					<div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/XochiquetzalliQuiroz.jpg" alt=""></div>
					<div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/GloriaSoancatl.jpg" alt=""></div>
					<div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/MarianaMassiel.jpg" alt=""></div>
					<div><img class="big-img" src="<?php echo get_template_directory_uri()?>/images/MiguelCristales.jpg" alt=""></div> -->
					<?php endwhile; else: 
					?>
					<?php endif; 
					?>

				</div>

				<div class="pl-5 pr-5 mt-4">
					<div class="slider-nav">
						<?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<div><img class="small-img" src="<?php echo $url?>" alt=""></div>
						<!-- <div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/JorgeAdolfo.jpg" alt=""></div> -->
						<!-- <div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/AlejandroPadilla.jpg" alt=""></div>
						<div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/CristinaReinosa.jpg" alt=""></div>
						<div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/XochiquetzalliQuiroz.jpg" alt=""></div>
						<div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/GloriaSoancatl.jpg" alt=""></div>
						<div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/MarianaMassiel.jpg" alt=""></div>
						<div><img class="small-img" src="<?php echo get_template_directory_uri()?>/images/MiguelCristales.jpg" alt=""></div> -->
						<?php endwhile; else: 
						?>
						<?php endif; 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
