<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href="https://fonts.googleapis.com/css?family=Nunito:300,400" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/slick/slick.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/slick/slick-theme.css">
		<!--Title-->
		<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
