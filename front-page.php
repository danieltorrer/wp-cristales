<?php get_header(); ?>

<div class="container-fluid">
	<?php include 'template-header.php' ?>
</div>

<!--header-->
<div class="container-fluid d-flex align-items-center justify-content-center slider">
	<img src="<?php echo get_template_directory_uri()?>/images/logo-white.png" alt="" class="wow fadeIn">
</div>

<!--servicios-->
<div class="logo-xl ">
	<div class="overlay-computer"></div>

	<div class="container">
		<div class="row pt-5">
			<div class="col-12">
				<img src="<?php echo get_template_directory_uri()?>/images/texto.png" alt="" class="mt-5 pt-5 wow fadeIn">
			</div>
		</div>
	</div>

</div>


<div class="container-fluid servicios" id="servicios">
	<div class="container">
		<div class="row text-center text-white d-flex justify-content-center h-100">
			<div class="col-lg-3 menu-startup pt-5 pb-5 px-0 " data-toggle="modal" data-target="#startupModal">
				<div class="mt-5 pb-5 wow fadeIn">
					<div class="menu-title">
						<h5>STARTUP</h5>
					</div>
					<svg class="mt-4 mb-4 icon-svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					height="60px" viewBox="0 0 47.247 34.07" enable-background="new 0 0 47.247 34.07" xml:space="preserve">
						<g>
							<g>
								<path fill="#FFFFFF" d="M35.355,0.343C30.957,1.734,29.429,6.295,29.69,8.2c0.08-0.058,0.16-0.115,0.24-0.179
								c2.96-2.234,6.316-3.325,9.738-3.955c1.557-0.288,2.619-0.341,2.619-0.341s-8.807,3.193-12.098,5.67
								c-3.767,2.829-5.53,7.471-6.567,12.522c-1.032-5.052-2.8-9.693-6.562-12.522C13.771,6.925,4.964,3.731,4.964,3.731
								s1.062,0.052,2.62,0.341c3.421,0.629,6.783,1.721,9.738,3.955c0.08,0.063,0.16,0.115,0.24,0.179
								c0.255-1.905-1.272-6.466-5.666-7.857C7.108-1.164,2.545,2.854,0,2.687c3.506,2.893,3.997,5.739,9.092,8.291
								c3.461,1.731,5.791,0.739,7.128-0.555c0.03,0.035,4.248,3.037,6.402,12.783c-4.718,0.468-8.396,4.18-8.396,8.694
								c0,2.928,18.8,2.858,18.8,0c0-4.515-3.678-8.227-8.391-8.694c2.144-9.758,6.361-12.754,6.392-12.788
								c1.333,1.293,3.667,2.286,7.128,0.554c5.095-2.552,5.591-5.404,9.092-8.291C44.708,2.849,40.134-1.176,35.355,0.343L35.355,0.343z
								M35.355,0.343"/>
							</g>
						</g>
					</svg>

					<p>
						Servicios para emprendedores <br>
						que están en etapa de idea <br>
						y desean iniciar un proyecto <br>
						o adquirir nuevas <br>
						habilidades y conocimientos
					</p>
				</div>
			</div>
			<div class="col-lg-3 menu-startup pt-5 pb-5 px-0" data-toggle="modal" data-target="#viveroModal">
				<div class="mt-5 pb-5 bottom-to-top wow fadeIn">
					<div class="menu-title">
						<h5>VIVERO <br> DE EMPRESAS</h5>
					</div>

					<svg class="mt-4 mb-4 icon-svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					height="60px" viewBox="0 0 33.695 33.728" enable-background="new 0 0 33.695 33.728" xml:space="preserve">
						<g>
							<path fill="#FFFFFF" d="M16.848,0L0,10.783v22.944h6.147l0,0H5.391V23.601l-0.534,0.001
							c-1.918,0.003-3.476-1.551-3.479-3.47l-0.001-0.773l0.773-0.001c1.473-0.002,3.111,0.913,3.619,2.207
							c0.507-1.294,2.146-2.209,3.619-2.207l0.773,0.001l-0.001,0.773c-0.003,1.919-1.56,3.473-3.479,3.47l-0.534-0.001v10.127l0,0h5.385
							l0,0h-0.757V17.47l-0.534,0.001c-1.918,0.002-3.476-1.552-3.479-3.47l-0.001-0.773l0.773-0.002
							c1.473-0.001,3.111,0.914,3.619,2.207c0.507-1.293,2.146-2.208,3.619-2.206l0.773,0.001l-0.001,0.773
							c-0.003,1.919-1.56,3.472-3.479,3.47l-0.534-0.001v16.258l0,0h5.28l0,0h-0.343V23.601l-0.534,0.001
							c-1.919,0.003-3.477-1.551-3.479-3.47l-0.001-0.773l0.773-0.001c1.446-0.002,3.045,0.882,3.583,2.138V21.48
							c0.012,0.027,0.024,0.055,0.036,0.083v0.001c0.38-0.971,1.397-1.728,2.503-2.045c0.369-0.105,0.747-0.162,1.115-0.162l0.774,0.001
							l0,0l0,0l-0.002,0.773c0,0.24-0.024,0.475-0.071,0.7c-0.233,1.131-1.015,2.061-2.055,2.498c-0.416,0.176-0.873,0.272-1.353,0.271
							l-0.534-0.001v10.127l0,0h5.716l0,0h-0.757V17.47l-0.534,0.001c-1.918,0.002-3.476-1.552-3.479-3.47l-0.001-0.773l0.773-0.002
							c1.474-0.001,3.112,0.914,3.619,2.207c0.507-1.293,2.146-2.208,3.618-2.206l0.773,0.001l-0.001,0.773
							c-0.003,1.919-1.56,3.472-3.479,3.47l-0.534-0.001v16.258l0,0h5.651l0,0h-0.757V23.601l-0.533,0.001
							c-1.919,0.003-3.477-1.551-3.479-3.47l-0.001-0.773l0.773-0.001c1.474-0.002,3.111,0.913,3.619,2.207
							c0.507-1.294,2.146-2.209,3.618-2.207l0.773,0.001l-0.001,0.773c-0.003,1.919-1.56,3.473-3.479,3.47l-0.534-0.001v10.127l0,0h5.103
							V10.783L16.848,0z"/>
							<path fill="#549459" d="M21.24,19.358L21.24,19.358l-0.774-0.001c-0.368,0-0.746,0.057-1.115,0.162
							c0.369-0.105,0.747-0.162,1.115-0.162L21.24,19.358z"/>
							<path fill="#549459" d="M19.351,19.52c-1.105,0.317-2.123,1.074-2.503,2.045v-0.001v0.001
							C17.228,20.594,18.245,19.837,19.351,19.52z"/>
							<path fill="#36605E" d="M16.812,21.48v0.015c0.01,0.023,0.026,0.045,0.036,0.068C16.836,21.535,16.824,21.508,16.812,21.48z"/>
						</g>
					</svg>


					<p>Acompañamiento de emprendedores <br>
						con prototipos realizados y a punto <br>
						de iniciar una empresa o de  <br>
						microempresarios con una apertura
					</p>
				</div>
			</div>
			<div class="col-lg-3 menu-startup pt-5 pb-5 px-0" data-toggle="modal" data-target="#medianaModal">
				<div class="mt-5 pb-5 wow fadeIn">
					<div class="menu-title">
						<h5>MEDIANAS Y <br>GRANDES <br>EMPRESAS</h5>
					</div>

					<svg class="mt-4 mb-4 icon-svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					height="60px" viewBox="0 0 43.438 41.27" enable-background="new 0 0 43.438 41.27" xml:space="preserve">
						<g opacity="0.53">
							<g>
								<path fill="#FFFFFF" d="M25.214,2.391c-0.185-0.289-0.244-0.484-0.244-0.484l0.088-0.082c0,0,0.175,0.073,0.476,0.197
								c0.143,0.058,0.316,0.127,0.517,0.204c0.197,0.073,0.411,0.155,0.642,0.212c0.111,0.035,0.229,0.07,0.347,0.083
								c0.059,0.008,0.119,0.024,0.177,0.035c0.06,0.005,0.117,0.008,0.177,0.015c0.236,0.03,0.456,0.005,0.695-0.007
								c0.11-0.041,0.234-0.053,0.354-0.105c0.112-0.064,0.257-0.102,0.359-0.187c0.055-0.038,0.109-0.075,0.172-0.115
								c0.063-0.035,0.13-0.063,0.197-0.135l0.422-0.356c0.004-0.005-0.025,0.037-0.011,0.018l0.011-0.01l0.012-0.01l0.03-0.025
								l0.055-0.05l0.109-0.098l0.21-0.199c0.132-0.135,0.3-0.242,0.432-0.371c0.13-0.135,0.307-0.22,0.446-0.35
								c0.149-0.117,0.324-0.207,0.491-0.321c0.354-0.165,0.769-0.29,1.14-0.237c0.092,0.01,0.183,0.025,0.271,0.04
								c0.085,0.024,0.168,0.055,0.247,0.085c0.162,0.052,0.305,0.129,0.437,0.206c0.265,0.152,0.477,0.33,0.644,0.502
								s0.295,0.339,0.384,0.486c0.096,0.143,0.15,0.27,0.189,0.356c0.033,0.087,0.048,0.135,0.048,0.135l-0.087,0.084
								c0,0-0.192-0.064-0.484-0.187c-0.142-0.057-0.313-0.13-0.514-0.21c-0.192-0.077-0.401-0.164-0.624-0.239
								c-0.111-0.042-0.227-0.085-0.341-0.111c-0.06-0.016-0.117-0.038-0.175-0.055c-0.06-0.011-0.12-0.025-0.177-0.046
								c-0.239-0.077-0.486-0.154-0.794-0.187c-0.139,0.021-0.306,0.013-0.454,0.072c-0.139,0.072-0.299,0.127-0.413,0.254
								c-0.112,0.122-0.242,0.229-0.322,0.385l-0.117,0.231l-0.053,0.117L30.154,1.99l-0.012,0.029l-0.005,0.014L30.135,2.04
								l-0.003,0.005c0.011-0.018-0.017,0.027-0.017,0.025l-0.228,0.501c-0.075,0.162-0.246,0.347-0.387,0.506
								C29.359,3.254,29.17,3.354,29,3.491c-0.179,0.125-0.394,0.197-0.594,0.287C27.985,3.9,27.544,3.92,27.173,3.823
								c-0.19-0.042-0.365-0.097-0.52-0.177c-0.16-0.067-0.3-0.152-0.429-0.234c-0.26-0.173-0.467-0.354-0.629-0.534
								C25.429,2.706,25.307,2.536,25.214,2.391L25.214,2.391z M43.438,30.916V41.27H0V30.916h2.958V19.281l8.34-5.849v5.849l8.335-5.849
								v5.849l8.338-5.849v11.497h2.877l1.564-20.338h4.769l1.366,20.338h3.207v5.986H43.438z M13.617,26.443H7.061v6.051h6.557V26.443z
								M24.913,26.443h-6.558v6.051h6.558V26.443z M36.351,26.443h-6.558v6.051h6.558V26.443z M36.351,26.443"/>
							</g>
						</g>
					</svg>
					<p>
						Servicios destinados a la Mediana <br>
						y Gran Empresa para atender <br>
						el fondeo de Proyectos específicos, <br>
						la Capacitación In situ de <br>
						su personal y soluciones a la <br>
						medida de problemas específicos
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'template-modals.php'; ?>

<div id="clientes-satisfechos"></div>
<div class="clientes pb-5 pt-5">
	<div class="container pt-5">
		<div class="row">
			<div class="col-12 pl-5">
				<h2 class="pl-5">CLIENTES <span class="blue-font serif">SATISFECHOS</span></h2>
				<h2 class="ml-5 pl-5">Y <span class="blue-font serif">LIDERES</span> DE OPINION</h2>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row mt-5">
		<div class="col-12">
			
			<?php 
				$args = array('post_type' => 'video');
				$the_query = new WP_Query( $args);

				if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
			?>
			<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="false">
				<div class="carousel-inner">

					<?php 
						for ($i=0; $i < get_count_field("url"); $i++) {
		    	           	$index = $i + 1;
		        	       	$field = get_field("url");
		        	      	if($i % 3 == 0){
		        	       		if($i == 0){?>
						    		<div class="carousel-item active">
	        	        	<?php
	        	        		} else { ?>
						    		<div class="carousel-item">
		       	        	<?php }
		       	        	}
		          	    	echo $field[$index];
		       		        if($i % 3 == 2 || $i+1 == get_count_field("url")) { ?>
							   	</div>
		    	            <?php
	        	        	}
	            		}
					?>


				</div>
			</div>

			<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
		    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    	<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
	    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
	    		<span class="sr-only">Next</span>
	  		</a>
		</div>

		<?php endwhile; else: ?>
		<?php endif; ?>
	</div>
</div>
<div class="container d-flex justify-content-center mt-5 mb-3">
	<div class="row">
		<div class="col-12">
			<img src="<?php echo get_template_directory_uri()?>/images/hands.png" alt="hands" class="responsive-image">
		</div>
	</div>
</div>


<?php include 'template-aliados.php';?>

<?php include 'template-equipo.php'; ?>


<div id="filosofia-empresarial"></div>
<div class="filosofia pt-5">
	<div class="container pt-5">
		<div class="blue-half-1"></div>
		<div class="blue-half-2"></div>
		<div class="row">
			<!-- <div class="col-12 ml-5 pl-5 hidden-mobile">
				<div class="wow fadeIn">
					<h1>FILOSOFÍA</h1>
					<h1 class="ml-5">EMPRESARIAL</h1>
				</div>
			</div> -->
			<div class="col-12 ">
				<div class="wow fadeIn">
					<h1>FILOSOFÍA</h1>
					<h1 class="ml-5">EMPRESARIAL</h1>
				</div>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-lg-6 d-flex justify-content-end pills-container">
				<div class="nav flex-column nav-pills filosofia-pills text-center" id="v-pills-tab" role="tablist">
					<a class="pr-5 serif nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-expanded="true">MISIÓN</a>
					<a class="pr-5 serif nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-expanded="true">VISIÓN 2022</a>
					<a class="pr-5 serif nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-expanded="true">PROPUESTA DE VALOR</a>
					<a class="pr-5 serif nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true">VALORES</a>
				</div>
			</div>
			<div class="col-lg-4">

				<div class="filosofia-tab-content tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"><p class="pl-5">Ayudar a los empresarios a crear, consolidar o mejorar sus organizaciones con resultados precisos y visibles </p></div>
					<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab"><p class="pl-5">Miguel Cristales y Consultores es el despacho que tiene el mayor reconocimiento y el mayor número de financiamientos obtenidos en el Estado de Puebla.</p></div>
					<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab"><p class="pl-5">¡Porque creemos que <br>algunas locuras cambian <br> al mundo, creemos en ti, <br> cristalizamos tus sueños! </p></div>
					<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab"><ul class="white-font pl-5">
						<li>Honestidad</li>
						<li>Pasión</li>
						<li>Transparencia</li>
						<li>Innovación</li>
						<li>Trascendencia</li>
						<li>Profesionalismo</li>
						<li>Liderazgo</li>
						<li>Estrategia</li>
						<li>Trabajo en Equipo</li>
						<li>Enfoque de resultados</li>
					</ul></div>
				</div>
			</div>
		</div>

	</div>
</div>


<?php get_footer(); ?>
