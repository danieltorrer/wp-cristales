<?php 
	$args = array('post_type' => 'aliado');
	$the_query = new WP_Query( $args);
?>

<div id="aliados-estrategicos"></div>
<div class="container-fluid aliados">
	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col 8 d-flex justify-content-start pl-5">
				<h2><span class="serif gray-font">ALIADOS</span><span class="serif blue-font"> ESTRATÉGICOS</span></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-6 text-center mt-5">
				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
					<?php $i = 0; ?>
					<?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

						<li class="nav-item">
							<a class="nav-link" id="pills-tab-aliado-<?php echo $i?>" data-toggle="pill" href="#pills-aliado-<?php echo $i?>" role="tab" aria-controls="pills-aliado-<?php echo $i?>" aria-selected="false">
								<img src="<?php echo $url?>">
							</a>
						</li>
						<?php $i++; ?>

					<?php endwhile; else: 
					?>
					<?php endif; 
					?>

				</ul>
			</div>

			<div class="col-6">
				<div class="tab-content" id="pills-tabContent">
				
					<?php $i = 0; ?>
					<?php if( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
												
						<div class="tab-pane fade" id="pills-aliado-<?php echo $i?>" role="tabpanel" aria-labelledby="pills-tab-aliado-<?php echo $i?>">
							<h4 class="tt-u"><?php the_title(); ?></h4> <br>
							<p><?php the_content(); ?></p>
							<button 
								data-modal-title="<?php the_title(); ?>" 
								data-modal-description="<?php echo get_field("servicios")[1]; ?>" 
								type="button" 
								class="btn tt-u btn-border modal-aliado-button" 
								data-toggle="modal" 
								data-target="#emailModal-2">
								Estoy interesado
							</button>
						</div>

						<?php $i++; ?>

					<?php endwhile; else: 
					?>
					<?php endif; 
					?>

				<!-- 	<div class="tab-pane fade" id="pills-aliado-3" role="tabpanel" aria-labelledby="pills-tab-aliado-3">
						<h4 class="tt-u">SPELZ MAGIC</h4>
						<p>Servicios de magia para
eventos especiales y venta
de productos para mago.</p>
						<button type="button" class="btn tt-u btn-border" data-toggle="modal" data-target="#emailModal-2">Estoy interesado</button>
					</div>

					<div class="tab-pane fade" id="pills-aliado-4" role="tabpanel" aria-labelledby="pills-tab-aliado-4">
						<h4 class="tt-u">Revive Spaces</h4> <br>
<p>Empresa dedicada la remodelación de espacios con servicio de pintura, carpintería, tablarocas, etcétera</p>

						<button type="button" class="btn tt-u btn-border" data-toggle="modal" data-target="#emailModal-2">Estoy interesado</button>
					</div> -->

				</div>
			</div>
		</div>

	</div>
</div>
