


<div class="container-fluid footer pt-5 pb-4">
	<div class="row pt-5 pb-5">
		<div class="col-lg-6 d-flex justify-content-end align-items-center pr-5 contacto-social">
			<a href="https://www.facebook.com/miguelcristalesyconsultores/"><img src="<?php echo get_template_directory_uri(); ?>/images/f-blue-icon.png" alt="" class="mr-2 pr-2 border-right"></a>
			<a href="https://www.twitter.com/MACRISTALES/"><img src="<?php echo get_template_directory_uri(); ?>/images/t-blue-icon.png" alt="" class="mr-2 pr-2 border-right tw-icon"></a>
			<a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-cristales-hoyos-46416b44/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/in-blue-icon.png" alt="" class="pr-2"></a>
			<a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-cristales-hoyos-46416b44/" class="blue  border-right pr-1" target="_blank"><span>Curriculum</span></a>
			<a href="https://www.youtube.com/channel/UCWE_v8BGxXgk2wH6-h32rtQ"><img src="<?php echo get_template_directory_uri(); ?>/images/yt-blue-icon.png" alt="" class="pr-2 pl-2 yt-icon"></a>
		</div>
		<div class="col-lg-6 contacto-info">
			<div class="col-lg-6">
				<div class="mb-2 inblock">
					<a href="mailto:cristales.miguel@gmail.com"><img src="<?php echo get_template_directory_uri(); ?>/images/mail-blue-icon.png" alt="" class="mr-2 align-top mt-1"></a><a href="" class="blue"><p class="inblock mc-direccion">cristales.miguel@gmail.com</p></a>
				</div>
				<div></div>
				<div class="mb-2 inblock">
					<a href="https://goo.gl/maps/uuxb42j4zzt" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/location-blue-icon.png" alt="" class="margin-1 align-top inblock"><p class="blue inblock mc-direccion">Blvd. Atlixcáyotl 1499, Reserva Territorial Atlixcáyotl, San Andrés Cholula, Puebla 72810</p></a>
				</div>
				<div></div>
				<div class="mb-2 inblock">
					<a href="https://goo.gl/maps/offQmBRvSvM2" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/location-blue-icon.png" alt="" class="margin-1 align-top inblock"><p class="blue inblock mc-direccion">21 Poniente 908A, Col. Insurgentes Chula Vista, Puebla, Puebla.</p></a>
				</div>
				<div></div>
				<div class="mb-2 inblock">
					<a href="tel:4788394"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-blue-icon.png" alt="" class="margin-1 inblock"><p class="blue inblock mc-direccion">4.78.83.94</p></a>
				</div>
				<div></div>
				<div class="mb-2 inblock">
					<a href="https://api.whatsapp.com/send?phone=2224868401"><img src="<?php echo get_template_directory_uri(); ?>/images/wa-icon.png" alt="" class="inblock wa-icon"><p class="blue inblock mc-direccion">2224.86.84.01</p></a>
				</div>
			</div>
		</div>

		<div class="col-12 text-center mt-5">
			<a href="<?php echo site_url()?>/aviso-de-privacidad/">Aviso de Privacidad</a>
		</div>
	</div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/slick/slick.min.js"></script>
<script>  new WOW().init(); </script>
<script>
	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		centerMode: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-for-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		centerMode: false,
		asNavFor: '.slider-for, .slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: true,
		arrows: true,
		centerMode: false,
		asNavFor: '.slider-for, .slider-for-2',
		focusOnSelect: true
	});

	$('.videos-slick').slick({
  		infinite: true,
  		slidesToShow: 3,
  		slidesToScroll: 3,
 		dots: true,
	});

	$('#soluciones-link').click( function(){
		$('#medianaModal').modal('hide')
		document.getElementById('aliados-estrategicos').scrollIntoView();
	});

	$('.modal-aliado-button').click( function(e){
		var title = $(e.target).attr('data-modal-title');
		var description = $(e.target).attr('data-modal-description');

		$('.aliados-form .data-modal-title').val(title);
		$('.aliados-form .data-modal-description').val(description);

	})
			
</script>
<?php wp_footer(); ?>

</body>
</html>
