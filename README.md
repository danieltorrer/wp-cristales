# Pagina web Cristales

La pagina web Cristales es un tema wordpress creado a la medida a partir de los disenos PSD. 

# Instalacion
- Es necesario que el dominio cuente con wordpress pre-instalado. La mayoria de los proveedores de hosting cuentan con plataformas para instalar wordpress con un click.

- Descargue este repositorio como ZIP https://gitlab.com/danieltorrer/wp-cristales/-/archive/master/wp-cristales-master.zip

- Ir al panel de admin de wordpress e instale el tema. Una guia mas detallada se encuentra aqui: https://ayudawp.com/como-instalar-temas-en-wordpress/

- Si se esta migrando de un hosting a otro es importante que respalde la informacion del sitio viejo y la importe en el nuevo, de otra manera las secciones no estaran correctamente configuradas. Guia a detalle: https://www.webempresa.com/blog/exportar-importar-post-paginas-wordpress-formato-zip.html
