<div class="row fixed-top fixed-header pt-3 pb-3">
	<div class="col-3 d-flex justify-content-center">
		<div class="dropdown mt-4">
			<button class="mc-hamburguer" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<img src="<?php echo get_template_directory_uri()?>/images/hamburguer.png" alt="">
			</button>
			<div class="dropdown-menu mc-dropdown" aria-labelledby="dropdownMenuButton">
				<h3 class="mc-dropdown-title sans"><a href="#servicios">Servicios</a></h3>
				<div class="line"></div>
				<a class="dropdown-item text-center mc-dropdown-item" href="#clientes-satisfechos">Clientes satisfechos <br>y líderes de opinión</a>
				<a class="dropdown-item text-center mc-dropdown-item" href="#aliados-estrategicos">Aliados Estratégicos</a>
				<a class="dropdown-item text-center mc-dropdown-item" href="#filosofia-empresarial">Filosofía empresarial</a>
				<a class="dropdown-item text-center mc-dropdown-item" href="#equipo">Equipo de Consultores</a>
				<div class="line"></div>
				<a class="dropdown-item text-center mc-dropdown-item" href="#">Tienda virtual <br>MACH 51</a>
			</div>
		</div>
	</div>
	<div class="col-6">
		<a href="<?php echo site_url()?>"><img src="<?php echo get_template_directory_uri()?>/images/logo-color.png" alt="" height="60" class="mr-auto ml-auto d-flex justify-content-center"></a>
	</div>
	<div class="col-3 d-flex align-items-center justify-content-center header-social">
		<a href="https://www.facebook.com/miguelcristalesyconsultores/" target="_blank" class="d-none d-md-block">
			<img src="<?php echo get_template_directory_uri()?>/images/f-icon.png" alt="" class=" pr-2 mr-2 border-right">
		</a>
		<a href="https://www.twitter.com/MACRISTALES/">
			<img src="<?php echo get_template_directory_uri(); ?>/images/t-icon.png" alt="" class="pr-2 pr-2 tw-icon">
		</a>
		<a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-cristales-hoyos-46416b44/" target="_blank" class="d-none d-md-block">
			<img src="<?php echo get_template_directory_uri()?>/images/in-icon.png" class="border-left pl-2">
		</a>
		<a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-cristales-hoyos-46416b44/" class="mc-cv d-none d-md-block" target="_blank">
				<span>Curriculum</span>
		</a>
		<span class="border-right"></span>
		<a href="mailto:cristales.miguel@gmail.com" class="d-none d-md-block">
			<img src="<?php echo get_template_directory_uri()?>/images/mail-icon.png" alt="" class="ml-2 pl-2 pr-2 border-left">
		</a>
		<a href="https://www.youtube.com/channel/UCWE_v8BGxXgk2wH6-h32rtQ">
			<img src="<?php echo get_template_directory_uri(); ?>/images/yt-icon.png" alt="" class="pl-2 pr-2 border-left yt-icon">
		</a>
	</div>
</div>
