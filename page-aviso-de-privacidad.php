<?php get_header(); ?>

<!--servicios-->
<div class="container-fluid ">

	<div class="container">
		<?php include 'template-header.php' ?>
	</div>

	<div class="container">
		<div class="col-12 aviso-privacidad">
			<h3>AVISO DE PRIVACIDAD DE MIGUEL CRISTALES Y CONSULTORES</h3>

			<p>Miguel Cristales y Consultores con domicilio en Vía Atlixcayotl 1499, Reserva Territorial Atlixcayotl, Tercer piso del edificio City Angelópolis, San Andrés Cholula, Puebla, CP. 73810; es la responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:</p>

			<p>Los datos personales que recabamos, se utilizarán para la finalidad única de proporcionarle el servicio, atención, consultoría y asistencia que Miguel Cristales y Consultores le otorga, conforme a sus servicios, como son: Talleres, consultoría, vivero de empresas, conferencias, networkings, platicas empresariales, entrevistas, coaching, mentorías, visitas empresariales, asesorías o ayuda para intercambios comerciales. Así como la implementación e impulso de otros programas de emprendimiento y todo lo que conlleve a estos objetivos.</p>

			<p>Para otorgarle la atención, apoyo y servicios a través de Miguel Cristales y Consultores, se requerirán y utilizarán datos personales de identificación, contacto, laborales y patrimoniales. Adicionalmente, pudiéramos obtener información individualizada de fuentes de terceros que a su vez se obligan en cumplir con la legislación aplicable en materia de privacidad y se agrega a la información de usted que se mantiene en la Base de Datos de Miguel Cristales y Consultores.</p>

			<p>Manifestamos que los datos personales de menores de edad, personas en estado de interdicción y capacidades diferentes, en términos de ley, serán recabados a través de sus padres o tutores y se implementarán las medidas de seguridad más estrictas a efecto de asegurar la confidencialidad de los menores y este grupo de personas.</p>

			<p>Los datos por Usted proporcionados serán concentrados y tratados profesionalmente para el solo fin de determinar el apoyo, intervención, atención y/o viabilidad de acceder a los servicios otorgados por Miguel Cristales y Consultores, y no serán compartidos o trasladados a terceros, sino con su consentimiento y salvo en los casos que así se amerite y requiera por el servicio, consultoría o asistencia que se proporcione o por medio de la solicitud de una autoridad judicial pertinente.</p>

			<p>Miguel Cristales y Consultores se compromete a que sus datos serán tratados bajo medidas de seguridad, garantizando su resguardo; respetando en todo momento el derecho a la identidad y confidencialidad como elementos fundamentales del ejercicio de los Derechos Humanos.</p>

			<p>De conformidad con el artículo 8 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, se entiende que usted consiente tácitamente el tratamiento de sus datos personales cuando se pone a su disposición el presente Aviso de Privacidad, y usted no manifiesta su oposición.</p>

			<p>Usted podrá negar su consentimiento en cualquier momento sin que se le atribuyan efectos retroactivos, lo cual puede realizar a través de los medios y procedimientos implementados por Miguel Cristales y Consultores.</p>

			<p>Miguel Cristales y Consultores hace de su conocimiento los casos en que no será necesario el consentimiento para el tratamiento de los datos personales de acuerdo con lo dispuesto en el artículo 10 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares que establece:</p>
			<ul>
				<li type="I">Esté previsto en una ley.</li>
				<li type="I">Los datos figuren en fuentes de acceso público;</li>
				<li type="I">Los datos personales se sometan a un procedimiento previo de disociación;</li>
				<li type="I">Tenga el propósito de cumplir obligaciones derivadas de una relación jurídica entre el titular y el responsable;</li>
				<li type="I">Exista una situación de emergencia que potencialmente pueda dañar a un individuo en su persona o en sus bienes;</li>
				<li type="I">Sean indispensables para la atención médica, la prevención, diagnóstico, la prestación de asistencia sanitaria, tratamientos médicos o la gestión de servicios sanitarios, mientras el titular no esté en condiciones de otorgar el consentimiento, en los términos que establece la Ley General de Salud y demás disposiciones jurídicas aplicables y que dicho tratamiento de datos se realice por una persona sujeta al secreto profesional u obligación equivalente, o</li>
				<li type="I">Se dicte resolución de autoridad competente.</li>
			</ul>

			<p>Igualmente se estará a lo previsto por el artículo 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, en tratándose de transferencias nacionales e internacionales de datos.</p>

			<p>De conformidad a la normatividad de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, usted puede hacer valer sus derechos de acceder, rectificar y cancelar sus datos personales, así como oponerse al tratamiento de los mismos o revocar el consentimiento que para tal fin nos haya otorgado(derecho ARCO). Para ello, puede contactarnos a través del departamento administrativo de Miguel Cristales y Consultores con domicilio en Vía Atlixcayotl 1499, Reserva Territorial Atlixcayotl, Tercer piso del edificio City Angelópolis, San Andrés Cholula, Puebla, CP. 73810 y, número telefónico 01(222)4 78 83 94, así como a través del correo electrónico <a href="cristales.miguel@gmail.com">cristales.miguel@gmail.com</a></p>


			<p>Cualquier modificación a este aviso de privacidad podrá ser consultada en el historial del Aviso de Privacidad, en la página electrónica <a href="http://www.miguelcristales.com.mx">www.miguelcristales.com.mx</a></p>
		</div>
	</div>

</div>


<?php get_footer(); ?>
